const { Car } = require('../models/index');
const moment = require('moment');
class CarController{

    //get all data cars
    static getAll(req, res){
        Car.findAll({
            order:[
                ['id','DESC']
            ]
        })
        .then(cars => {
            // res.status(200).json(cars);
            res.render('index', { cars, moment });
        })
        .catch(err => {
            res.send(err);
        })
    }

    //filter data by size cars
  static filter(req, res){
        Car.findAll({
            where:{
                size: req.params.size
            }
        })
        .then(cars => {
            res.render('filter',{cars, size: req.params.size, moment});
        })
        .catch(err => {
            res.send(err);
        })
    }

    //create data cars
     static create(req, res){
        Car.create({
            name: req.body.name,
            type: req.body.type,
            price: req.body.price,
            size:  req.body.size,
            image: req.file.filename,
            created_at: new Date(),
            updated_at: new Date()
        })
        .then(car => {
                // res.status(201).json(teacher);
                    res.redirect('/');
            })
            .catch(err => {
                res.send(err);
            })
    };

    //update pages cars
    static updated(req, res){
       Car.findByPk(req.params.id)
        .then(car => {
            res.render('update', { car });
        })
        .catch(err => {
            res.send(err);
        })
    }

    //update action
    static actionUpdate(req, res, moment){

        if(req.file){
        Car.update(req.body,
            {
                name: req.body.name,
                type: req.body.type,
                price: req.body.price,
                size:  req.body.size,
                image: req.body.image = req.file.filename,
                updated_at: new Date(),
            where:{
                id: req.params.id
            }

        })
        .then(car => {
            res.redirect('/');
        })
        .catch(err => {
            res.send(err);
        })

        } else {
             Car.update(req.body,
            {
                name: req.body.name,
                type: req.body.type,
                price: req.body.price,
                size:  req.body.size,
                image: req.body.oldImage,
                updated_at: moment(),
            where:{
                id: req.params.id
            }
        })
        .then(car => {
            res.redirect('/');
        })
        .catch(err => {
            res.send(err);
        })
        }
    }


    //delete data

    static deleted(req, res){
        Car.destroy({
            where:{
                id: req.params.id
            }
        })
            .then(cars => {
                 res.redirect('/');
            })
            .catch(err => {
                res.send(err);
            })
    }
}

module.exports = CarController;