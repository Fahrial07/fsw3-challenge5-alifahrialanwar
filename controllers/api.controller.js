const { Car } = require('../models/index');

class ApiController{

    static getAll(req, res){
        Car.findAll({
            order:[
                ['id','DESC']
            ]
        })
        .then(cars => {
            res.status(200).json(cars);
        })
        .catch(err => {
            res.send(err);
        })
    }
    static getById(req, res){
        Car.findByPk(req.params.id)
            .then(car => {
                res.status(200).json(car);
            })
            .catch(err => {
                res.send(err);
            })
    }


    static filter(req, res){
        Car.findAll({
            where:{
                size: req.params.size
            }
        })
        .then(cars => {
            res.status(200).json(cars);
        })
        .catch(err => {
            res.send(err);
        })
    }

    static create(req, res){
        Car.create({
             name: req.body.name,
            type: req.body.type,
            price: req.body.price,
            size:  req.body.size,
            image: req.file.filename,
            created_at: new Date(),
            updated_at: new Date()
        })
        .then(car => {
            res.status(200).json(car);
        })
        .catch(err => {
            res.send(err);
        })
    }

    static update(req, res){
        if(req.file){
        Car.update(req.body,
            {
                name: req.body.name,
                type: req.body.type,
                price: req.body.price,
                size:  req.body.size,
                image: req.body.image = req.file.filename,
                updated_at: new Date(),
            where:{
                id: req.params.id
            }

        })
        .then(car => {
              res.status(200).json(car);
        })
        .catch(err => {
            res.send(err);
        })

        } else {
             Car.update(req.body,
            {
                name: req.body.name,
                type: req.body.type,
                price: req.body.price,
                size:  req.body.size,
                image: req.body.oldImage,
                updated_at: new Date(),
            where:{
                id: req.params.id
            }
        })
        .then(car => {
              res.status(200).json(car);
        })
        .catch(err => {
            res.send(err);
        })
        }
    }

    static delete(req, res){
        Car.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(car => {
            res.status(200).json(car);
        })
        .catch(err => {
            res.send(err);
        })
    }

}

module.exports = ApiController;