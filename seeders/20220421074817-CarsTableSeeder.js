'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

   await queryInterface.bulkInsert('Cars', [
     {
      name: 'Vios',
      type: 'Sedan',
      price: '400,000',
      size: 'Medium',
      image: 'car-example.png',
       created_at: new Date(),
      updated_at: new Date()
     },
    {
      name: 'Civic',
      type: 'Sedan',
      price: '800,000',
      size: 'Small',
      image: 'car-example.png',
       created_at: new Date(),
      updated_at: new Date()
    },
    {
      name: 'Honda Jazz',
      type: 'Sedan',
      price: '500,000',
      size: 'Medium',
      image: 'car-example.png',
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      name: 'Toyota Corolla',
      type: 'Sedan',
      price: '350,000',
      size: 'Medium',
      image: 'car-example.png',
      created_at: new Date(),
      updated_at: new Date()
    },
  ], {})
},

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Cars', null, {});
  }
};
