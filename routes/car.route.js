const express = require('express');
const Controller = require('../controllers/cars.controller');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const moment = require('moment');

const storage = multer.diskStorage({
    // destination: `./public/img`,
    destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "../public/img"))
  },
    filename: (req, file, cb) => {
        const fileName = `${Date.now()}${file.originalname.toLowerCase().split(' ').join('-')}`;
        //callback
        cb(null, fileName);

    }
});
const uploadImage =  multer({storage: storage});

//get all data
router.get('/', (req, res) => {
    Controller.getAll(req, res);
});

//filter data by size cars
router.get('/filter/:size', (req, res) => {
    Controller.filter(req, res);
})

//create data cars
router.post('/create', uploadImage.single('image'), (req, res) => {
    Controller.create(req, res);
});


//route delete data car
router.get('/deleted/:id', (req, res) => {
    Controller.deleted(req, res);
});


//udpate data cars
//get data by id for update
router.get('/update/:id', uploadImage.single('image'), (req, res) => {
    Controller.updated(req, res);
});

//action update car
router.post('/update/:id', uploadImage.single('image'), (req, res) => {
    Controller.actionUpdate(req, res);
})

//pages create cars
router.get('/create-car',  (req, res) => {
    res.render('create');
})



module.exports = router;
