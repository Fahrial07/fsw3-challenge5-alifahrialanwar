const express = require('express');
const Controller = require('../controllers/api.controller');
const router = express.Router();

const path = require('path');
const multer = require('multer');

const storage = multer.diskStorage({
    // destination: `./public/img`,
    destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "../public/img"))
  },
    filename: (req, file, cb) => {
        const fileName = `${Date.now()}${file.originalname.toLowerCase().split(' ').join('-')}`;
        //callback
        cb(null, fileName);

    }
});
const uploadImage =  multer({storage: storage});


router.get('/getAll', (req, res) => {
    Controller.getAll(req, res);
});

router.get('/getById/:id', (req, res) => {
    Controller.getById(req, res);
});

router.get('/filter/:size', (req, res) => {
    Controller.filter(req, res);
});

router.post('/create', uploadImage.single('image'), (req, res) => {
    Controller.create(req, res);
});

router.put('/update/:id', uploadImage.single('image'), (req, res) => {
    Controller.update(req, res);
});

router.delete('/delete/:id', (req, res) => {
    Controller.delete(req, res);
});

module.exports = router;