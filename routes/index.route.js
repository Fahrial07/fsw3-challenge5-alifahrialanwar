const express = require('express');
const router = express.Router();

const carsApiRoute  = require('./api.route');
const carsRoute = require('./car.route');

//routes
// router.get('/', (req, res) => {
//     res.render('index');
// })

router.use('/carsApi', carsApiRoute);
router.use('/', carsRoute);

module.exports = router;
