//show hide sidebar
function openCloseSidebar()
{
   let side = document.getElementById("sidebar-box");
   let content = document.getElementById("content-cars");

   if(side.style.display === "none"){
       side.style.display = 'block';
       content.style.marginLeft = '260px';
   } else {
        side.style.display = 'none';
        content.style.marginLeft = '0px';
   }
}
