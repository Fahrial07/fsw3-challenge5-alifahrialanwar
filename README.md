# fsw3-challernge5-Ali Fahrial Anwar

FSW3 - Challenge5 - Ali Fahrial Anwar



## Introduction
Pada aplikasi ini terdapat 2 routes yaitu untuk menjalankan halaman login dan juga api.

## ERD

![ERD](/ERD.png)


## Pages Route

```bash
1. Home / Index
"http://localhost:3000/"
2. Add Car
"http://localhost:3000/create-car"
3. Update Car
"http://localhost:3000/update-car/:id"
4. Delete Car
"http://localhost:3000/deleted/:id"
4. Filter Car
"http://localhost:3000/filter/"
  {
      filter : Small, Large, Medium
  }
```


## API ROUTE
1. Get all data cars

```bash
"http://localhost:3000/carsApi/getAll"
```
    Response
```bash
    [
    {
        "id": 10,
        "name": "Xenia",
        "type": "V",
        "price": "400000",
        "size": "Medium",
        "image": "1650645719754car-example.png",
        "created_at": "2022-04-22T16:41:59.757Z",
        "updated_at": "2022-04-22T16:41:59.757Z"
    },
    {
        "id": 9,
        "name": "Toyota Avanza",
        "type": "G",
        "price": "450000",
        "size": "Medium",
        "image": "1650645682535car-example.png",
        "created_at": "2022-04-22T16:41:22.872Z",
        "updated_at": "2022-04-22T16:41:22.874Z"
    },
    {
        "id": 5,
        "name": "Kijang Innova",
        "type": "Reborn",
        "price": "500000",
        "size": "Medium",
        "image": "1650642779526car-example.png",
        "created_at": "2022-04-22T15:52:59.892Z",
        "updated_at": "2022-04-22T15:52:59.893Z"
    }
]
```

2. Get data car by id

```bash
"http://localhost:3000/carsApi/getById/:id"
```
Response

```bash
{
    "id": 5,
    "name": "Kijang Innova",
    "type": "Reborn",
    "price": "500000",
    "size": "Medium",
    "image": "1650642779526car-example.png",
    "created_at": "2022-04-22T15:52:59.892Z",
    "updated_at": "2022-04-22T15:52:59.893Z"
}
```

3. Get Cars By Filter

```bash
"http://localhost:3000/carsApi/filter/:size"

"Filter [ 'Small', 'Medium', 'Large' ]"
```
Response

```bash
[
    {
        "id": 5,
        "name": "Kijang Innova",
        "type": "Reborn",
        "price": "500000",
        "size": "Medium",
        "image": "1650642779526car-example.png",
        "created_at": "2022-04-22T15:52:59.892Z",
        "updated_at": "2022-04-22T15:52:59.893Z"
    },
    {
        "id": 9,
        "name": "Toyota Avanza",
        "type": "G",
        "price": "450000",
        "size": "Medium",
        "image": "1650645682535car-example.png",
        "created_at": "2022-04-22T16:41:22.872Z",
        "updated_at": "2022-04-22T16:41:22.874Z"
    },
    {
        "id": 10,
        "name": "Xenia",
        "type": "V",
        "price": "400000",
        "size": "Medium",
        "image": "1650645719754car-example.png",
        "created_at": "2022-04-22T16:41:59.757Z",
        "updated_at": "2022-04-22T16:41:59.757Z"
    }
]
```

4. Add Car

```bash
"http://localhost:3000/create"
```
Response

```bash
{
    "id": 12,
    "name": "Innova ",
    "type": "Limited Edition",
    "price": "650000",
    "size": "Large",
    "image": "1650646474885car-example.png",
    "created_at": "2022-04-22T16:54:34.915Z",
    "updated_at": "2022-04-22T16:54:34.915Z"
}
```

4. Update Car
```bash
"http://localhost:3000/carsApi/update/:id"
```
Response

```bash
[
    1
]
```

4. Delete Car

```bash
"http://localhost:3000/carsApi/delete/:id"
```
Resposne

```bash
    1
```

## Screenshot of the project

###  1. Main Page

![ERD](/main.PNG)

###  2. Filter Page

![ERD](/filter.PNG)


